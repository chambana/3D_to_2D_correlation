**Given a location in 3D space (floating point X,Y, and Z coordinates), determine the location of that point on a 2D 
image (X and Y pixel locations).  This technique can be expanded to correlate clusters of 3D points to a 2D 
“bounding box” on a frame.**


**Project Overview Slides:**
https://docs.google.com/presentation/d/15TD0occIjp5gdPoJxwAwBl7v0n5YvA54GbvQtf7fWEU/edit?usp=sharing






**Overview of the 3D LIDAR data to 2D Camera frame:**
![](demo.png)