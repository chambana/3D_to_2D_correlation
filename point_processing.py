'''

OVERVIEW:  
Code for correlating a 3D point in space (X,Y,Z) to a pixel on a 2D frame
Field of View values assume a Logitech c920 camera
-Select video1 or video0 to choose the appropriate device on your system
-Alter THREE_D_POINT_FROM_LIDAR to correlate different 3D points to the fram

Written by Drew Watson
MIT License

'''


import cv2
import os
from math import *



'''
REFERENCE:

Coordinate System of LIDAR

+Y
|
|
+Z_____+X


Coordinate System of CAMERA

_____+X
|
|
+Y

'''




# The real-world 3D point to test and correlate to a point on the frame
THREE_D_POINT_FROM_LIDAR = (-6.8884, 34.997, 1.6344)


#SET FRAME WIDTH TO VALUES WITH KNOWN FIELD OF VIEW VALUES FOR THE LOGITECH C920
WIDTH = 1920
HEIGHT = 1080

#FOV VAVLUES FOR THE LOGITECH C920 WHEN OPERATING AT 1920X1080
HFOV=70.42
VFOV=43.30

#CALCULATE THE PIXE-TO-DEGREE RATIO
VERT_NUM_PIXELS_PER_DEGREE = HEIGHT / VFOV
HORIZ_NUM_PIXELS_PER_DEGREE = WIDTH / HFOV

print('VERT_NUM_PIXELS_PER_DEGREE',VERT_NUM_PIXELS_PER_DEGREE)
print('HORIZ_NUM_PIXELS_PER_DEGREE',HORIZ_NUM_PIXELS_PER_DEGREE)




'''
UNCOMMENT IF USING LIVE CAPTURE
'''
#SET THE CAMERA'S FRAME SETTINGS IN VIDEO FOR LINUX2
# os.system("v4l2-ctl -d /dev/video1 --set-fmt-video=width=1920,height=1080,pixelformat=1")
# os.system("v4l2-ctl -d /dev/video1 --set-parm=30")
#OPEN CAPTURE DEVICE (0 IS BUILTIN WEBCAM, 1 IS LOGITECH C920)
# cap = cv2.VideoCapture(0)
# ret, frame = cap.read()
#SET FRAME WIDTH/HEIGHT
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, WIDTH)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)


# MAKE A TEST GRID OVERLAY TO SHOW THE LOCATIONS OF VARIOUS HORIZ/VERTICAL ANGULAR OFFSETS
test_grid = []
for x in range(-20,21):
    for z in range(-10,11):
        test_grid.append((x,20,z))


#get phi (horizontal) offset from frame center
def point_to_horizontal_angle(point):
    phi_deg = degrees(atan2(point[0], point[1]))  #X and Y values
    return phi_deg

#get number of pixels offset from the frame center
def horizontal_angle_to_pixel(angle_deg):
    horiz_offset_from_origin_in_pixels = angle_deg * HORIZ_NUM_PIXELS_PER_DEGREE
    return horiz_offset_from_origin_in_pixels


#get theta (vertical) offset from frame center
def point_to_vertical_angle(point):
    theta_deg = degrees(atan2(point[2],point[1]))  #Z and Y values
    return theta_deg

#get number of pixels offset from the frame center
def vertical_angle_to_pixel(angle_deg):
    vert_offset_from_origin_in_pixels = angle_deg * VERT_NUM_PIXELS_PER_DEGREE
    return vert_offset_from_origin_in_pixels

# high level function to do full 3D to 2D conversion
def get_2Dpixel_from_3Dpoint(pt):
    vert_angle_deg = point_to_vertical_angle(pt)
    vert_pixel_offset_from_center = vertical_angle_to_pixel(vert_angle_deg)
    vert_pixel_location_on_screen = int((HEIGHT / 2) - vert_pixel_offset_from_center)  # negative angle means negative pixel offset.  This should push you FURTHER down the screen so subtract a negative

    horiz_angle_deg = point_to_horizontal_angle(pt)
    horiz_pixel_offset_from_center = horizontal_angle_to_pixel(horiz_angle_deg)
    horiz_pixel_location_on_screen = int((WIDTH / 2) + horiz_pixel_offset_from_center)  # add here don't subtract.  positive phi value produces positive pixel offset. pixels grow left to right

    pixel_screen_location = (horiz_pixel_location_on_screen, vert_pixel_location_on_screen)

    return pixel_screen_location,horiz_angle_deg,vert_angle_deg



# while(cap.isOpened()):
while 1:

    # ret, frame = cap.read()   #live capture

    #USE THIS TO OVERLAY CORRELATED POINT ON PRE-CAPTURED FRAME VICE LIVE VIDEO
    frame = cv2.imread('/home/drew/lidar_collections/tallbldg.jpg')  #show 3D point in 2D frame

    # Draw the test grid on the frame for reference
    for  index, three_d_pt in enumerate(test_grid):

        print("drawing piont", three_d_pt, index)

        #get a pixel location and angular offsets for each point in the test grid/map
        pixel_location, horiz_angle_deg, vert_angle_deg = get_2Dpixel_from_3Dpoint(three_d_pt)
        print('pixel_location, horiz_angle_deg, vert_angle_deg',pixel_location, horiz_angle_deg, vert_angle_deg)
        #draw a labeled circle at each grid mapping point that identifies it's angular offsets
        cv2.circle(frame, pixel_location, 5, (255,0,0),  thickness=-1)
        cv2.putText(frame, str(int(horiz_angle_deg))+' '+str(int(vert_angle_deg)), pixel_location,  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), thickness=2)


    #translate the 3D point to 2D pixels
    threeD_point_2D_pixel_location, threeD_point_horiz_angle, threeD_point_vertical_angle = get_2Dpixel_from_3Dpoint(THREE_D_POINT_FROM_LIDAR)
    print(threeD_point_2D_pixel_location, threeD_point_horiz_angle, threeD_point_vertical_angle)

    # Draw the 3D test point's assessed location on the 2D frame.
    cv2.circle(frame, threeD_point_2D_pixel_location, 15, (255, 255, 0), thickness=-1)

    # Label the 3D test point's exact calculated Horizontal and Vertical angle offset
    cv2.putText(frame, str(threeD_point_horiz_angle) + ' ' + str(threeD_point_vertical_angle), threeD_point_2D_pixel_location,
                cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 255, 0), thickness=5)

    cv2.imshow("Mapping of Camera's Field of View angles",frame)

    # Hit the "q" key to quit the application
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
