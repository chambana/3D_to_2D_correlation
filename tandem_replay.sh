#!/bin/sh
echo "Launching ROSCORE, RVIZ, IMAGE_SAVER"

gnome-terminal -x roscore
gnome-terminal -x rosrun rviz rviz -f velodyne -d ~/.rviz/tandem_compressedvideo_and_lidar.rviz
gnome-terminal -x rosbag play -l ~/lidar_collections/tandem_outside3-8.7.17.bag
gnome-terminal -x rosrun image_view image_saver image:=/usb_cam/image_raw compressed _save_all_image:=true _filename_format:=last_frame.jpg __name:=image_saver

 
